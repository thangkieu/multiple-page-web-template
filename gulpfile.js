// generated on 2016-03-15 using generator-gulp-webapp 1.1.1
var gulp = require('gulp');
var hb = require('gulp-hb');

var gulpLoadPlugins = require('gulp-load-plugins');
var browserSync = require('browser-sync');
var del = require('del');
var hash_src = require('gulp-hash-src');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const path = {
   app: 'app',
   dist: 'dist',
   tmp: '.tmp',
   scripts: 'app/js',
   styles: 'app/scss',
   tmpScripts: '.tmp/js',
   tmpStyles: '.tmp/css',
   distScripts: 'dist/js',
   distStyles: 'dist/css',
   templates: 'app/templates',
   data: 'app/data'
};

gulp.task('styles', () => {
  return gulp
    .src(path.styles + '/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe(
      $.sass
        .sync({
          outputStyle: 'expanded',
          precision: 10,
          includePaths: ['.']
        })
        .on('error', $.sass.logError)
    )
    .pipe($.autoprefixer({ browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'] }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(path.tmpStyles))
    .pipe(reload({ stream: true }));
});

gulp.task('scripts', () => {
  return gulp
    .src(path.scripts + '/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.plumber())
    .pipe(
      $.babel({
        presets: ['@babel/env']
      })
    )
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(path.tmpScripts))
    .pipe(reload({ stream: true }));
});

function lint(files, options) {
  return () => {
    return gulp
      .src(files)
      .pipe(reload({ stream: true, once: true }))
      .pipe($.eslint(options))
      .pipe($.eslint.format())
      .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
  };
}
const testLintOptions = {
  env: {
    mocha: true
  }
};

gulp.task('lint', lint(path.scripts + '/**/*.js'));
gulp.task('lint:test', lint('test/spec/**/*.js', testLintOptions));

gulp.task('build:js', () =>
  gulp
    .src(path.scripts + '/**/*.js')
    .pipe(
      $.babel({
        presets: ['@babel/env']
      })
    )
    .pipe($.uglify())
    .pipe(gulp.dest(path.distScripts))
);

gulp.task('build:css', ['styles'], () =>
  gulp
    .src(path.tmpStyles + '/main.css')
    .pipe($.cssnano())
    .pipe(gulp.dest(path.distStyles))
);

gulp.task('build:html', ['tpl'], () => gulp.src('.tmp/index.html').pipe(gulp.dest(path.dist)));

gulp.task('build:assets', ['build:js', 'build:css', 'build:html']);

gulp.task('images', () => {
  return gulp
    .src('app/img/**/*')
    .pipe(
      $.if(
        $.if.isFile,
        $.cache(
          $.imagemin({
            progressive: true,
            interlaced: true,
            // don't remove IDs from SVGs, they are often used
            // as hooks for embedding and styling
            svgoPlugins: [{ cleanupIDs: false }]
          })
        ).on('error', function(err) {
          console.log(err);
          this.end();
        })
      )
    )
    .pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', () => {
  return gulp
    .src('app/fonts/**/*')
    .pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', () => {
  return gulp
    .src(['app/*.*', '!app/*.html'], {
      dot: true
    })
    .pipe(gulp.dest(path.dist));
});

gulp.task('clean', del.bind(null, ['.tmp', path.dist]));

gulp.task('serve', ['styles', 'scripts', 'fonts', 'tpl'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: [path.tmp, path.app]
    }
  });

  gulp
    .watch([path.app + '/*.html', path.tmpScripts + '/**/*.js', path.app + '/images/**/*', path.tmp + '/fonts/**/*'])
    .on('change', reload);

  gulp.watch(path.styles + '/**/*.scss', ['styles']);
  gulp.watch(path.scripts + '/**/*.js', ['scripts']);
  gulp.watch(path.templates + '/**/*.hbs', ['tpl']);
  gulp.watch(path.data + '/**/*.{js,json}', ['tpl']);
  gulp.watch(path.app + '/*.html', ['tpl']);
  gulp.watch(path.app + '/**/*.html', ['tpl']);
  gulp.watch(path.app + '/fonts/**/*', ['fonts']);
});

gulp.task('serve:dist', () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: [path.dist]
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/js'
      }
    }
  });

  gulp.watch(path.scripts + '/**/*.js', ['scripts']);
  gulp.watch('test/spec/**/*.js').on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});


/**
 * Register Handlebars
 */
gulp.task('tpl', () => {
  let hbStream = hb()
    .partials(path.templates + '/*.{hbs,js}')
    .helpers(path.app + '/helpers/*.js')
    .data(path.data + '/**/*.{js,json}');

  return gulp
    .src(path.app + '/*.html')
    .pipe($.plumber())
    .pipe(hbStream)
    .pipe(gulp.dest(path.tmp))
    .pipe(reload({ stream: true }));
});

gulp.task('s', ['serve']);
gulp.task('s:t', ['serve:test']);

gulp.task('build-src', ['lint', 'build:assets', 'images', 'fonts', 'extras'], () => {
  return gulp.src(path.dist + '/**/*').pipe($.size({ title: 'build', gzip: true }));
});

gulp.task('build', ['build-src'], () => {
  return gulp
    .src([path.dist + '/**/*.html', path.dist + '/**/*.css'])
    .pipe(hash_src({ build_dir: path.dist, src_path: path.dist }))
    .pipe(gulp.dest(path.dist));
});

// gulp.task('build', ['revision'], () => {
//   critical.generate({
//     inline: true,
//     base: 'dist/',
//     src: 'index.html',
//     dest: 'index.html',
//     minify: false,
//     width: 1300,
//     height: 900
//   });
// });

gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
